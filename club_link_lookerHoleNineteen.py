import time
import traceback
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.chrome.service import Service as ChromeService 
from webdriver_manager.chrome import ChromeDriverManager
from selenium.common.exceptions import TimeoutException
import psycopg2
import openpyxl
from random import randint
from selenium.webdriver.chrome.options import Options
import os
from travelMythLooker import random_wait, stopwatch

_conn_params = {
    'dbname': 'golfclubs',
    'user': 'golfuser',
    'password': 'calibri23',
    'host': 'localhost',
    'port': '5432'
}


def sql_quote_check(dict_raw_data):
    if dict_raw_data['address'] != None:
        if "'" in dict_raw_data['address']:
            dict_raw_data['address'] = dict_raw_data['address'].replace("'", "''")
    return dict_raw_data

def make_list_clubs():
    try:
        conn = psycopg2.connect(**_conn_params)
        cursor = conn.cursor()
        cursor.execute(

"""SELECT clubs.id, links.link 
FROM clubs 
JOIN links ON clubs.id = links.club_id
LEFT JOIN locations ON clubs.location_id = locations.id
WHERE locations.address IS NULL

;
"""
)
        results = cursor.fetchall()

        club_links = []
        for result in results:
            dict_club = {}
            dict_club['id'] = result[0]
            dict_club['link'] = result[1]
            club_links.append(dict_club)
        cursor.close()
        conn.close()
        print('list of clubs is created', len(club_links), 'clubs')
        return club_links
    except Exception as e:
        print('error in make_list_clubs')
        print(e)
        tb = traceback.format_exc()
        print(tb)
        return None
    


def get_address(club_link, driver):
    try:
        random_wait(3,4)

        try:
            """wait till address_data element is loaded"""
            address_data = WebDriverWait(driver, 1).until(
                EC.presence_of_element_located((By.XPATH, '//p[text()="Address"]/following-sibling::p[1]'))
            )

            if address_data is None:
                address_data = WebDriverWait(driver, 1).until(
                    EC.presence_of_element_located((By.CSS_SELECTOR, ' div > div > div > div > aside > section.course-side-info > ol > li > p.headline'))
                )
                print('css selector worked ', address_data.text)
            print('address =', address_data.text, ' of club =', club_link )
            if address_data is None or len(address_data.text)<3:
                return None
            return address_data.text if address_data else None

        except Exception as e:
            print('address_data is None in get_address , club_link =', club_link)
            return None

    except Exception as e:
        print(f"Error in get_address: {e}")
        return None

    
    
def get_email(driver_url):
    try:
      
        try:
            """wait till email_data element is loaded"""
            email_data = WebDriverWait(driver_url, 10).until(
                EC.presence_of_element_located((By.XPATH, '//p[text()="Email"]/following-sibling::a[1]'))
            )

            if email_data is None:
                email_data = WebDriverWait(driver_url, 10).until(
                    EC.presence_of_element_located((By.CSS_SELECTOR, ' div > div > div > div > aside > section.course-side-info > ol > li > a.headline'))
                )
                print('css selector worked ', email_data.text)

            print('email =', email_data.text )
            return email_data.text if email_data else None

        except Exception as e:
            print('email_data is None in get_email ')
            return None

    except Exception as e:
        print('error in get_email')
        print(e)
        tb = traceback.format_exc()
        print(tb)
  
        return None


    

def update_club_single(dict_club):
    try:
        conn = psycopg2.connect(**_conn_params)
        cursor = conn.cursor()
       
        club_id = dict_club['id']
        email = dict_club['email']
        address = dict_club['address']
        if address != None:
            address = dict_club['address']

            cursor.execute(f"""
            UPDATE locations 
            SET address = {address}'
            WHERE id = (
                SELECT location_id 
                FROM clubs 
                WHERE id = '{club_id}'
            );
        """)
        if email != None:
            cursor.execute(f"""
            UPDATE contacts 
            SET email = '{email}'
            WHERE id = (
                SELECT contact_id 
                FROM clubs 
                WHERE id = '{club_id}'
            );
                """)
        
        conn.commit()
        cursor.close()
        conn.close()
    except Exception as e:
        print('error in update_club_address')
        print(e)
        tb = traceback.format_exc()
        print(tb)
        return None


    

def main():
    chrome_options = webdriver.ChromeOptions()
    # chrome_options.add_argument("--headless")
    driver = webdriver.Chrome(options=chrome_options, service=ChromeService(ChromeDriverManager().install()))
    driver.maximize_window()
    try:
        stopwatch('on')
        list_dict_clubs = make_list_clubs()
        count_addresses = 0
        for dict_club in list_dict_clubs:
            club_link = dict_club['link']
            driver.get(club_link)
            address = get_address(club_link, driver)
            dict_club['address'] = address
            email = get_email(driver)
            dict_club['email'] = email

            dict_club=sql_quote_check(dict_club)
            count_addresses += 1
            update_club_single(dict_club)
        driver.close()
        print('count_addresses =', count_addresses)
        stopwatch('off')
    except Exception as e:
        print(e)
        tb = traceback.format_exc()
        print(tb)
        driver.close()
        return None
    

if __name__ == '__main__':
    print('club_link_lookerHoleNineteen.py is imported')
    main()
