

"""Import selenium and webdriver"""
import time
import traceback
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from time import sleep
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.chrome.service import Service as ChromeService 
from webdriver_manager.chrome import ChromeDriverManager
from selenium.common.exceptions import TimeoutException
import psycopg2
import openpyxl
from random import randint
from selenium.webdriver.chrome.options import Options
import os

basic_url= 'https://www.travelmyth.com/World/Hotels/five_star-golf'
check_avail_press_xpath = '//*[@id="select_dates_from_modal"]'
# next_page_xpath = "//span[@class='page-link']"
next_page_xpath =   '//*[@id="paging_footer"]/ul/li[last()]/span'
next_page_class = 'page-link'
next_page_css = 'ul > li:last-child > span.page_link'

address_press_class_name = 'fa fa-map-marker image_tag photo-tooltip show_on_map hotel_tab tabs_icons'
address_press_css = '.fa.fa-map-marker.image_tag.photo-tooltip.show_on_map.hotel_tab.tabs_icons'
address_class_name = 'map_content_field'
address_css = '[id^="tab-content-map-"] > div'

hotel_name_link_class_name = 'hotel_li_name_link ng-binding'
hotel_name_link_xpath = '//*[@id="64479"]/a'
hotel_name_link_css = '.hotel_li_name_link.ng-binding'

country_class_name = 'destination_no_link ng-binding'
country_xpath = '//*[@id="hotel_1"]/div/div/span[3]'
country_css='div > div > span:nth-child(4)'

city_class_name = 'destination_link ng-binding ng-scope'
city_xpath = '//*[@id="hotel_1"]/div/div/span[2]'
city_css = 'div > div > span:nth-child(3)'

check_in_press_class ='checkin_text'
date_start_november_press_xpath  = '/html/body/div[15]/div[2]/div/div[2]/div[1]/span[32]'
date_start_november_press_css = 'body > div.flatpickr-calendar.animate.multiMonth.arrowTop.arrowLeft.open > div.flatpickr-innerContainer > div > div.flatpickr-days > div:nth-child(1) > span:nth-child(32)'
date_end_november_press_xpath = '/html/body/div[16]/div[2]/div/div[2]/div[2]/span[9]'
date_end_november_press_css = 'body > div:nth-child(26) > div.flatpickr-innerContainer > div > div.flatpickr-days'
_conn_params = {
    'dbname': 'golfclubs',
    'user': 'golfuser',
    'password': 'calibri23',
    'host': 'localhost',
    'port': '5432'
}
_no_data_countries = []




list_no_data_country =[] 


def save_no_data_countries():
    global _no_data_countries
    with open('no_data_countriesUSA.txt', 'w') as filehandle:
        for listitem in _no_data_countries:
            filehandle.write('%s\n' % listitem)



def stopwatch(on_off_reset):
    global _start_time 
    if on_off_reset == 'on':
        _start_time = time.time()
    elif on_off_reset == 'off':
        end_time = time.time()
        print('time taken: ', (end_time - _start_time)/60)
    elif on_off_reset == 'reset':
        _start_time = time.time()


def random_wait(min, max):
    sleep(randint(min, max))
  

"""scrolling page to the specific element with xpath '//*[@id="paging_footer"]/ul/li[12]/span'"""
def scroll_to_element(driver, xpath):
    global next_page_status
    try:
        print('scroll_to_element')
        next_page_button = WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, xpath)))
        print('next_page_button ', next_page_button)
        driver.execute_script("arguments[0].scrollIntoView();", next_page_button)

        random_wait(2,3)
    except Exception as e:
        print('next page button is not found')
        # print(e)
        # tb = traceback.format_exc()
        # print(tb)
        next_page_status = False
        next_page_button = None
    return next_page_button


def get_hotel_links(driver_page):
    hotel_links = []
    try:
        hotel_links = driver_page.find_elements(By.CSS_SELECTOR,hotel_name_link_css)
        hotel_links = [{'name': link.text, 'link': link.get_attribute('href')} for link in hotel_links]
        print('hotel_links ', hotel_links)
    except Exception as e:
        print('get_hotel_links failed')
        # print(e)
        # tb = traceback.format_exc()
        # print(tb)
    return hotel_links


def dict_us_states(file_name):
    us_states = {}
    try:
        wb = openpyxl.load_workbook(file_name)
        sheet = wb['US States']
        for row in sheet.iter_rows():
            us_states[row[0].value] = [row[1].value, row[2].value]
    except Exception as e:
        print(e)
        tb = traceback.format_exc()
        print(tb)
    return us_states


def check_code_states(code, us_states):
    global _no_data_country
    state = {}
    try:
        if code in us_states.keys():
            state['country'] = 'USA'
            state['state'] = us_states[code][0]
            state['code'] = code
            state['continent'] = 'North America'
        else:
            print('state code not found ', code)
            _no_data_country.append(code)
    except Exception as e:
        print(e)
        tb = traceback.format_exc()
        print(tb)
    return state    


def make_dict_countries(file_name):
    countries = {}
    try:
        wb = openpyxl.load_workbook(file_name)
        sheet = wb['Countries']
        for row in sheet.iter_rows():
            countries[row[0].value] = [row[1].value, row[2].value]
    except Exception as e:
        print(e)
        tb = traceback.format_exc()
        print(tb)
    return countries


def check_code_countries(country, dict_countries):
    countr_dict = {}
    global _no_data_country
    try:
        if country in dict_countries.keys():
            countr_dict['country'] = country
            countr_dict['code'] = dict_countries[country][0]
            countr_dict['continent'] = dict_countries[country][1]
        else:
            print('country not found ', country)
            _no_data_country.append(country)
    except Exception as e:
        print(e)
        tb = traceback.format_exc()
        print(tb)
    return countr_dict


def accume_data(driver):
    try:
        list_raw_data = []
        random_wait(5, 8)
        dict_hotel_names_links = get_hotel_links(driver)
        list_countries = driver.find_elements(By.CSS_SELECTOR, country_css)
        list_cities = driver.find_elements(By.CSS_SELECTOR, city_css)
        for i in range(len(dict_hotel_names_links)):
            
            dict_raw_data = {}
            dict_raw_data['name'] = dict_hotel_names_links[i]['name']
            dict_raw_data['link'] = dict_hotel_names_links[i]['link']
            dict_raw_data['country'] = list_countries[i].text
            if dict_raw_data['country'] == 'United States':
                code = list_cities[i].text.split(' ')[-1]
                dict_raw_data['code'] = 'US '+ code
                dict_state = check_code_states(code, us_states)
                dict_raw_data['continent'] = 'North America'
                dict_raw_data['state'] = dict_state['state']
                dict_raw_data['country'] == 'USA'
            else:
                dict_country = check_code_countries(dict_raw_data['country'], dict_countries)
                dict_raw_data['code'] = dict_country['code']
                dict_raw_data['continent'] = dict_country['continent']
            list_raw_data.append(dict_raw_data)
    except Exception as e:
        print(e)
        tb = traceback.format_exc()
        print(tb)   
    return list_raw_data


def accume_data_usa(driver, country, state):
    try:
        list_raw_data = []
        # random_wait(5, 8)
        dict_hotel_names_links = get_hotel_links(driver)
        for i in range(len(dict_hotel_names_links)):
            
            dict_raw_data = {}
            dict_raw_data['name'] = dict_hotel_names_links[i]['name']
            dict_raw_data['link'] = dict_hotel_names_links[i]['link']
            dict_raw_data['country'] = country
            """parse through us_states to find state code"""
            state = state.replace('-', ' ')
            """split state by space and remove last two words, then join back with space"""
            state = state.split(' ')
            state = state[:-2]
            state = ' '.join(state)
            if state in us_states.keys():
                code = us_states[state][0]
            else:
                print('state not found in us states = ', state)
                code = 'none'
            dict_raw_data['code'] = 'US '+ code
            dict_raw_data['continent'] = 'North America'
            dict_raw_data['state'] = state
            dict_raw_data['address'] = 'to_be_defined'
            list_raw_data.append(dict_raw_data)
    except Exception as e:
        print('error in accume_data_usa')
        print(e)
        tb = traceback.format_exc()
        print(tb)   
    return list_raw_data

def accume_data_country(driver, country):
    try:
        list_raw_data = []
        random_wait(5, 8)
        dict_hotel_names_links = get_hotel_links(driver)
        list_countries = driver.find_elements(By.CSS_SELECTOR, country_css)
        list_cities = driver.find_elements(By.CSS_SELECTOR, city_css)
        for i in range(len(dict_hotel_names_links)):
            
            dict_raw_data = {}
            dict_raw_data['name'] = dict_hotel_names_links[i]['name']
            dict_raw_data['link'] = dict_hotel_names_links[i]['link']
            if '-' in country:
                country = country.replace('-', ' ')   
            dict_raw_data['country'] = country
            dict_raw_data['code'] = dict_countries[country][0]
            dict_raw_data['continent'] = dict_countries[country][1]
            
            list_raw_data.append(dict_raw_data)
    except Exception as e:
        print(e)
        tb = traceback.format_exc()
        print(tb)   
    return list_raw_data


def save_db(list_raw_data):
    try:
        conn = psycopg2.connect(**_conn_params)
        cursor = conn.cursor()
        for dict_raw_data in list_raw_data:
            print('dict_raw_data in save_db ', dict_raw_data)
            print('name ', dict_raw_data['name'], 'address ', dict_raw_data['address'])
            if dict_raw_data['country'] == 'USA':
                cursor.execute(f"select * from hotels where name = '{dict_raw_data['name']}' and state_name = '{dict_raw_data['state']}'")
                if cursor.fetchone():
                    continue
                else:

                    cursor.execute(f"""insert into hotels (name, short_name, country_name, continent, address, state_name) values ('{dict_raw_data['name']}', 
                                   '{dict_raw_data['code']}', '{dict_raw_data['country']}', '{dict_raw_data['continent']}', '{dict_raw_data['address']}', '{dict_raw_data['state']}')
                                   RETURNING *;""")
                    hotel_record = cursor.fetchone()
                    hotel_id = hotel_record[0]
                    
                    """, 
                    create new record in table links, where hotel_id = hotel_id, link = dict_raw_data['link']"""
                    cursor.execute(f"insert into links (hotel_id, link) values ('{hotel_id}', '{dict_raw_data['link']}')")


        
            else:
                cursor.execute(f"""select * from hotels where name = '{dict_raw_data['name']}' and country_name = '{dict_raw_data['country_name']}'""")
                if cursor.fetchone():
                    continue
                else:
                    cursor.execute(f"""insert into hotels (name, short_name, country_name, continent, address) values ('{dict_raw_data['name']}', '{dict_raw_data['code']}', '{dict_raw_data['country']}', '{dict_raw_data['continent']}', '{dict_raw_data['address']}')
                                   RETURNING *;""")
                    hotel_record = cursor.fetchone()
                    hotel_id= hotel_record[0]
                    cursor.execute(f"insert into links (hotel_id, link) values ('{hotel_id}', '{dict_raw_data['link']}')")


        conn.commit()
        cursor.close()
        conn.close()
    except Exception as e:
        print(e)
        tb = traceback.format_exc()
        print(tb)
    return list_raw_data

def sql_quote_check(dict_raw_data):
    if "'" in dict_raw_data['name']:
        dict_raw_data['name'] = dict_raw_data['name'].replace("'", "''")
    if "'" in dict_raw_data['address']:
        dict_raw_data['address'] = dict_raw_data['address'].replace("'", "''")
    return dict_raw_data

def save_db_w_no_address(list_raw_data):
    try:
        conn = psycopg2.connect(**_conn_params)
        cursor = conn.cursor()
        for dict_raw_data in list_raw_data:
            print('dict_raw_data in save_db ', dict_raw_data)
            dict_raw_data = sql_quote_check(dict_raw_data)
            print('name ', dict_raw_data['name'], 'address ', dict_raw_data['address'])
            if dict_raw_data['country'] == 'USA':
                cursor.execute(f"select * from hotels where name = '{dict_raw_data['name']}' and state_name = '{dict_raw_data['state']}' and address is not null and address != 'to_be_defined'")
                if cursor.fetchone() is None:
                    cursor.execute(f"""insert into hotels (name, short_name, country_name, continent, address, state_name) values ('{dict_raw_data['name']}', 
                                   '{dict_raw_data['code']}', '{dict_raw_data['country']}', '{dict_raw_data['continent']}', '{dict_raw_data['address']}', '{dict_raw_data['state']}')
                                   RETURNING *;""")
                    hotel_record = cursor.fetchone()
                    hotel_id = hotel_record[0]
                    cursor.execute(f"insert into links (hotel_id, link) values ('{hotel_id}', '{dict_raw_data['link']}')")
                else:
                    continue
            else:
                cursor.execute(f"select * from hotels where name = '{dict_raw_data['name']}' and country_name = '{dict_raw_data['country']}' and address is not null and address != 'to_be_defined'")
                if cursor.fetchone() is None:
                    cursor.execute(f"""insert into hotels (name, short_name, country_name, continent, address) values ('{dict_raw_data['name']}', 
                                   '{dict_raw_data['code']}', '{dict_raw_data['country']}', '{dict_raw_data['continent']}', '{dict_raw_data['address']}')
                                   RETURNING *;""")
                    hotel_record = cursor.fetchone()
                    hotel_id = hotel_record[0]
                    cursor.execute(f"insert into links (hotel_id, link) values ('{hotel_id}', '{dict_raw_data['link']}')")
                else:
                    continue

        conn.commit()
        cursor.close()
        conn.close()
    except Exception as e:
        print('error in save_db_w_no_address')
        print(e)
        tb = traceback.format_exc()
        print(tb)
    return list_raw_data



def add_address(driver):

    try:
        list_addresses = []
        count = 0
        while True:
            try:
                address_press = driver.find_elements(By.CSS_SELECTOR, address_press_css)[count]
            except IndexError:
                break  # Exit the loop if no more elements are found

            driver.execute_script("arguments[0].scrollIntoView();", address_press)
            random_wait(5, 10)
            address_press.click()
            random_wait(5, 8)
            count += 1  #
        random_wait(2, 3)
        list_addresses_raw = driver.find_elements(By.CSS_SELECTOR, address_css)
        list_addresses_with_some_raw = [address.text for address in list_addresses_raw]
        list_addresses = [s.replace('Address: ', '') for s in list_addresses_with_some_raw if s.startswith('Address: ')]
    except Exception as e:
        print('erro in add_address')
        print(e)
        tb = traceback.format_exc()
        print(tb)
    print('count Addresses', count)
    return list_addresses


def dict_continent_countries_maker(dict_countries, continent):
    dict_continent_countries = {}
    for key, value in dict_countries.items():
        if value[1] == continent:
            if " " in key:
                key = key.replace(" ", "-")
            dict_continent_countries[key] = value
    return dict_continent_countries


"""function list_states_maker makes list of states from dict_us_states"""
def list_states_maker(dict_us_states):
    list_states = []
    for key, value in dict_us_states.items():
        value[0] = value[0].strip()
        if " " in value[0]:
            value[0] = value[0].replace(" ", "-")
        list_states.append(value[0])
    return list_states


def date_input(driver):
    try:
        input_calendar_press_element = WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.CLASS_NAME, check_in_press_class)))
        input_calendar_press_element.click()
        random_wait(1, 2)
        date_start_november_press_element = WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, date_start_november_press_xpath)))
        date_start_november_press_element.click()
        random_wait(5, 8)
        date_end_november_press_element = WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, date_end_november_press_xpath)))
        date_end_november_press_element.click()
        random_wait(15,20)

    except Exception as e:
        print(e)
        tb = traceback.format_exc()
        print(tb)
        driver.quit()
        print('date_input failed')
    return


def runner_country(driver, country_state):
    global _no_data_countries
    global count_hotels
    
    try:
        random_wait(5,6)
        check_avail_press_element = WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, check_avail_press_xpath)))
        if check_avail_press_element == None:
            print('check_avail_press_element is None on  country ', country_state)
            return
    except Exception as e:
        print('country ', country_state, ' is not found')
        _no_data_countries.append(country_state)
        return
    
    try:

        date_input(driver)
        next_page_status = True
        count=0
        while next_page_status == True:
            if country_state in list_states:
                list_raw_data = accume_data_usa(driver, "USA", country_state)
                print('accume_data_usa passed')
            else:
                list_raw_data = accume_data_country(driver, country_state)
                print('accume_data_country passed')   
            # list_addresses = add_address(driver)
            # print('add_address passed')

            # for i in range(len(list_addresses)):
            #     list_raw_data[i]['address'] = list_addresses[i]
            # """check if in list_raw_data there are hotels with no address key, add address = None"""
            # for dict_raw_data in list_raw_data:
            #     if 'address' not in dict_raw_data.keys():
            #         dict_raw_data['address'] = None
            # save_db(list_raw_data)
            save_db_w_no_address(list_raw_data)
            count_hotels += len(list_raw_data)
            next_page  = scroll_to_element(driver, next_page_xpath)
            if next_page == None:
                break
                print('next_page is None')
            next_page.click()
            random_wait(5, 8)
 
    except Exception as e:
        print(e)
        tb = traceback.format_exc()
        print(tb)
        driver.quit()
    return


if __name__ == "__main__":
        

    us_states = dict_us_states('prepare_links.xlsx')
    print('us_states ', us_states)
    dict_countries = make_dict_countries('prepare_links.xlsx')
    # list_states = list_states_maker(us_states)
    list_states = [  'Georgia-United-States','New-York-United-States','Washington-United-States']

    stopwatch('on')
    count_state =0
    count_hotels=0
    for state in list_states:
        chrome_options = webdriver.ChromeOptions()
        # chrome_options.add_argument("--headless")
        driver = webdriver.Chrome(options=chrome_options, service=ChromeService(ChromeDriverManager().install()))
        driver.maximize_window()
        # if count_state == 2:
        #     break
        # count_state += 1
        basic_url= (f'https://www.travelmyth.com/{state}/Hotels/five_star-golf')
        driver.get(basic_url)
        runner_country(driver, state)
        driver.quit()
    save_no_data_countries()
    print('_no_data_countries ', _no_data_countries)
    stopwatch('off')
    print('count_hotels ', count_hotels)
    driver.quit()
