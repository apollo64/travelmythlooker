

"""Import selenium and webdriver"""
import time
import traceback
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from time import sleep
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.chrome.service import Service as ChromeService 
from webdriver_manager.chrome import ChromeDriverManager
from selenium.common.exceptions import TimeoutException
import psycopg2
import openpyxl
from random import randint
from selenium.webdriver.chrome.options import Options
import os

basic_url= 'https://www.travelmyth.com/World/Hotels/five_star-golf'
check_avail_press_xpath = '//*[@id="select_dates_from_modal"]'
# next_page_xpath = "//span[@class='page-link']"
next_page_xpath =   '//*[@id="paging_footer"]/ul/li[last()]/span'
next_page_class = 'page-link'
next_page_css = 'ul > li:last-child > span.page_link'

address_press_class_name = 'fa fa-map-marker image_tag photo-tooltip show_on_map hotel_tab tabs_icons'
address_press_css = '.fa.fa-map-marker.image_tag.photo-tooltip.show_on_map.hotel_tab.tabs_icons'
address_class_name = 'map_content_field'
address_css = '[id^="tab-content-map-"] > div'

hotel_name_link_class_name = 'hotel_li_name_link ng-binding'
hotel_name_link_xpath = '//*[@id="64479"]/a'
hotel_name_link_css = '.hotel_li_name_link.ng-binding'

country_class_name = 'destination_no_link ng-binding'
country_xpath = '//*[@id="hotel_1"]/div/div/span[3]'
country_css='div > div > span:nth-child(4)'

city_class_name = 'destination_link ng-binding ng-scope'
city_xpath = '//*[@id="hotel_1"]/div/div/span[2]'
city_css = 'div > div > span:nth-child(3)'

check_in_press_class ='checkin_text'
date_start_november_press_xpath  = '/html/body/div[15]/div[2]/div/div[2]/div[1]/span[32]'
date_start_november_press_css = 'body > div.flatpickr-calendar.animate.multiMonth.arrowTop.arrowLeft.open > div.flatpickr-innerContainer > div > div.flatpickr-days > div:nth-child(1) > span:nth-child(32)'
date_end_november_press_xpath = '/html/body/div[16]/div[2]/div/div[2]/div[2]/span[9]'
date_end_november_press_css = 'body > div:nth-child(26) > div.flatpickr-innerContainer > div > div.flatpickr-days'
_conn_params = {
    'dbname': 'golfclubs',
    'user': 'golfuser',
    'password': 'calibri23',
    'host': 'localhost',
    'port': '5432'
}
_no_data_countries = []






def save_no_data_countries():
    global _no_data_countries
    with open('no_data_countriesThree.txt', 'w') as filehandle:
        for listitem in _no_data_countries:
            filehandle.write('%s\n' % listitem)



def stopwatch(on_off_reset):
    global _start_time 
    if on_off_reset == 'on':
        _start_time = time.time()
    elif on_off_reset == 'off':
        end_time = time.time()
        print('time taken: ', (end_time - _start_time)/60)
    elif on_off_reset == 'reset':
        _start_time = time.time()


def random_wait(min, max):
    sleep(randint(min, max))
  

"""scrolling page to the specific element with xpath '//*[@id="paging_footer"]/ul/li[12]/span'"""
def scroll_to_element(driver, xpath):
    global next_page_status
    try:
        print('scroll_to_element')
        next_page_button = WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, xpath)))
        print('next_page_button ', next_page_button)
        driver.execute_script("arguments[0].scrollIntoView();", next_page_button)

        random_wait(2,3)
    except Exception as e:
        print('next page button is not found')
        # print(e)
        # tb = traceback.format_exc()
        # print(tb)
        next_page_status = False
        next_page_button = None
    return next_page_button


def get_hotel_links(driver_page):
    hotel_links = []
    try:
        hotel_links = driver_page.find_elements(By.CSS_SELECTOR,hotel_name_link_css)
        hotel_links = [{'name': link.text, 'link': link.get_attribute('href')} for link in hotel_links]
        print('hotel_links ', hotel_links)
    except Exception as e:
        print('get_hotel_links failed')
        # print(e)
        # tb = traceback.format_exc()
        # print(tb)
    return hotel_links





def make_dict_countries(file_name, continent):
    countries = {}
    try:
        wb = openpyxl.load_workbook(file_name)
        sheet = wb['Countries']
        for row in sheet.iter_rows():
            if row[2].value == continent:
                countries[row[0].value] = [row[1].value, row[2].value]
    except Exception as e:
        print(e)
        tb = traceback.format_exc()
        print(tb)
    return countries


def check_code_countries(country, dict_countries):
    global _no_data_country
    countr_dict = {}

    try:
        if country in dict_countries.keys():
            countr_dict['country'] = country
            countr_dict['code'] = dict_countries[country][0]
            countr_dict['continent'] = dict_countries[country][1]
        else:
            print('country not found ', country)
            _no_data_country.append(country)
    except Exception as e:
        print(e)
        tb = traceback.format_exc()
        print(tb)
    return countr_dict



def accume_data_country(driver, country):
    try:
        list_raw_data = []
        random_wait(5, 8)
        dict_hotel_names_links = get_hotel_links(driver)
        if dict_hotel_names_links == []:
            print('dict_hotel_names_links is empty')
            return list_raw_data
        for i in range(len(dict_hotel_names_links)):
            
            dict_raw_data = {}
            dict_raw_data['name'] = dict_hotel_names_links[i]['name']
            dict_raw_data['link'] = dict_hotel_names_links[i]['link']
            if '-' in country:
                country = country.replace('-', ' ')   
            if country in dict_countries.keys():         
                dict_raw_data['country'] = country
                dict_raw_data['code'] = dict_countries[country][0]
                dict_raw_data['continent'] = continent
            else:
                print('country not found in dict_countries = ', country)
                dict_raw_data['country'] = 'to_be_defined'
                dict_raw_data['code'] = 'to_be_defined'
                dict_raw_data['continent'] = continent
            dict_raw_data['address'] = 'to_be_defined'
            list_raw_data.append(dict_raw_data)
    except Exception as e:
        print('accume_data_country failed')
        print(e)
        tb = traceback.format_exc()
        print(tb)   
    return list_raw_data


def sql_quote_check(dict_raw_data):
    if "'" in dict_raw_data['name']:
        dict_raw_data['name'] = dict_raw_data['name'].replace("'", "''")
    if "'" in dict_raw_data['address']:
        dict_raw_data['address'] = dict_raw_data['address'].replace("'", "''")
    return dict_raw_data

def save_db_w_no_address(list_raw_data):
    try:
        conn = psycopg2.connect(**_conn_params)
        cursor = conn.cursor()
        for dict_raw_data in list_raw_data:
            print('dict_raw_data in save_db ', dict_raw_data)
            dict_raw_data = sql_quote_check(dict_raw_data)
            print('name ', dict_raw_data['name'], 'address ', dict_raw_data['address'])
            if dict_raw_data['country'] == 'USA':
                cursor.execute(f"select * from hotels where name = '{dict_raw_data['name']}' and state_name = '{dict_raw_data['state']}' and address is not null and address != 'to_be_defined'")
                if cursor.fetchone() is None:
                    cursor.execute(f"""insert into hotels (name, short_name, country_name, continent, address, state_name) values ('{dict_raw_data['name']}', 
                                   '{dict_raw_data['code']}', '{dict_raw_data['country']}', '{dict_raw_data['continent']}', '{dict_raw_data['address']}', '{dict_raw_data['state']}')
                                   RETURNING *;""")
                    hotel_record = cursor.fetchone()
                    hotel_id = hotel_record[0]
                    cursor.execute(f"insert into links (hotel_id, link) values ('{hotel_id}', '{dict_raw_data['link']}')")
                else:
                    continue
            else:
                cursor.execute(f"select * from hotels where name = '{dict_raw_data['name']}' and country_name = '{dict_raw_data['country']}' and address is not null and address != 'to_be_defined'")
                if cursor.fetchone() is None:
                    cursor.execute(f"""insert into hotels (name, short_name, country_name, continent, address) values ('{dict_raw_data['name']}', 
                                   '{dict_raw_data['code']}', '{dict_raw_data['country']}', '{dict_raw_data['continent']}', '{dict_raw_data['address']}')
                                   RETURNING *;""")
                    hotel_record = cursor.fetchone()
                    hotel_id = hotel_record[0]
                    cursor.execute(f"insert into links (hotel_id, link) values ('{hotel_id}', '{dict_raw_data['link']}')")
                else:
                    continue

        conn.commit()
        cursor.close()
        conn.close()
    except Exception as e:
        print('error in save_db_w_no_address')
        print(e)
        tb = traceback.format_exc()
        print(tb)
    return list_raw_data



def dict_continent_countries_maker(dict_countries, continent):
    dict_continent_countries = {}
    for key, value in dict_countries.items():
        if value[1] == continent:
            if " " in key:
                key = key.replace(" ", "-")
            dict_continent_countries[key] = value
    return dict_continent_countries


"""function list_states_maker makes list of states from dict_us_states"""
def list_states_maker(dict_us_states):
    list_states = []
    for key, value in dict_us_states.items():
        value[0] = value[0].strip()
        if " " in value[0]:
            value[0] = value[0].replace(" ", "-")
        list_states.append(value[0])
    return list_states


def date_input(driver):
    try:
        input_calendar_press_element = WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.CLASS_NAME, check_in_press_class)))
        input_calendar_press_element.click()
        random_wait(1, 2)
        date_start_november_press_element = WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, date_start_november_press_xpath)))
        date_start_november_press_element.click()
        random_wait(1, 2)
        date_end_november_press_element = WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, date_end_november_press_xpath)))
        date_end_november_press_element.click()
        random_wait(10,11)

    except Exception as e:
        print(e)
        tb = traceback.format_exc()
        print(tb)
        driver.quit()
        print('date_input failed')
    return




def runner_country(driver, country_state):
    global _no_data_countries
    global count_hotels
    
    try:
        # random_wait(2,3)
        check_avail_press_element = WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, check_avail_press_xpath)))
        if check_avail_press_element == None:
            print('check_avail_press_element is None on  country ', country_state)
            return
    except Exception as e:
        print('country ', country_state, ' is not found')
        _no_data_countries.append(country_state)
        return
    
    try:

        date_input(driver)
        next_page_status = True
        count=0
        while next_page_status == True:
    
            list_raw_data = accume_data_country(driver, country_state)
            print('accume_data_country passed')   
            if list_raw_data == []:
                print('no data on country ', country_state)
                _no_data_countries.append(country_state)

            save_db_w_no_address(list_raw_data)
            count_hotels += len(list_raw_data)
            print('count_hotels ', count_hotels)
            next_page  = scroll_to_element(driver, next_page_xpath)
            if next_page == None:
                print('next_page is None')
                break
            next_page.click()
            random_wait(1, 2)
 
    except Exception as e:
        print(e)
        tb = traceback.format_exc()
        print(tb)
        driver.quit()
    return


continent = 'North America'
count_hotels=0
if __name__ == "__main__":

    dict_countries = make_dict_countries('prepare_links.xlsx',continent)
    
    print('dict_countries ', dict_countries)
    list_countries = list(dict_countries.keys())
    list_countries = [ country.replace(' ', '-') for country in list_countries]
    stopwatch('on')
    count_countries =0
    
    for country in list_countries:
        print('country ', country)
        chrome_options = webdriver.ChromeOptions()
        # chrome_options.add_argument("--headless")
        driver = webdriver.Chrome(options=chrome_options, service=ChromeService(ChromeDriverManager().install()))
        driver.maximize_window()
        # if count_countries == 2:
        #     break
        # count_countries += 1
        basic_url= (f'https://www.travelmyth.com/{country}/Hotels/five_star-golf')
        driver.get(basic_url)
        runner_country(driver, country)
        driver.quit()
    save_no_data_countries()
    print('_no_data_countries ', _no_data_countries)
    stopwatch('off')
    print('count_hotels ', count_hotels)
    print('count_countries ', count_countries)
