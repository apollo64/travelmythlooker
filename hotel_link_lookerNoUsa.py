import time
import traceback
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.chrome.service import Service as ChromeService 
from webdriver_manager.chrome import ChromeDriverManager
from selenium.common.exceptions import TimeoutException
import psycopg2
import openpyxl
from random import randint
from selenium.webdriver.chrome.options import Options
import os
from travelMythLooker import random_wait, stopwatch

_conn_params = {
    'dbname': 'golfclubs',
    'user': 'golfuser',
    'password': 'calibri23',
    'host': 'localhost',
    'port': '5432'
}


def sql_quote_check(dict_raw_data):
    if "'" in dict_raw_data['hotel_name']:
        dict_raw_data['hotel_name'] = dict_raw_data['hotel_name'].replace("'", "''")
    if "'" in dict_raw_data['address']:
        dict_raw_data['address'] = dict_raw_data['address'].replace("'", "''")
    return dict_raw_data

def make_list_hotels():
    try:
        conn = psycopg2.connect(**_conn_params)
        cursor = conn.cursor()
        cursor.execute("""
    SELECT hotels.id, hotels.name, links.link 
    FROM hotels 
    JOIN links ON hotels.id = links.hotel_id
    WHERE hotels.address = 'to_be_defined'
        """)
        results_usa = cursor.fetchall()

        hotel_links = []
        for result in results_usa:
            dict_hotel = {}
            
            dict_hotel['hotel_id'] = result[0]
            dict_hotel['hotel_name'] = result[1]
            dict_hotel['link'] = result[2]
            
            hotel_links.append(dict_hotel)
        cursor.close()
        conn.close()
        print('list of hotels is created', len(hotel_links), 'hotels')
        return hotel_links
    except Exception as e:
        print('error in make_list_hotels')
        print(e)
        tb = traceback.format_exc()
        print(tb)
        return None
    


def get_address(hotel_link, driver):
    try:
        driver.get(hotel_link)
        random_wait(2,3)
        try:
            city = driver.find_element(By.XPATH,'//*[@id="location_details"]/a[1]')
            city = city.text
            
        except:
            city = ''
            print('city is None in get_address , hotel_link =', hotel_link)
        try:
            first_part_address = driver.find_element(By.XPATH,'//*[@id="address_text"]')
            first_part_address = first_part_address.text
        except:
            first_part_address = ''
            print('first_part_address is None in get_address , hotel_link =', hotel_link)   
            
        address = first_part_address +', '+ city
        print('address =', address, ' of hotel =', hotel_link )
        if len(address) < 4:
            address = 'not_defined_by_travelMyth'
        return address
    except Exception as e:
        print('error in get_address')
        print(e)
        tb = traceback.format_exc()
        print(tb)
        return None
    

def update_hotel_address(list_dict_hotels):
    try:
        conn = psycopg2.connect(**_conn_params)
        cursor = conn.cursor()
        for dict_hotel in list_dict_hotels:
            hotel_id = dict_hotel['hotel_id']
            address = dict_hotel['address']
            cursor.execute(f"""
            UPDATE hotels 
            SET address = '{address}'
            WHERE id = '{hotel_id}'
        """)
        conn.commit()
        cursor.close()
        conn.close()
    except Exception as e:
        print('error in update_hotel_address')
        print(e)
        tb = traceback.format_exc()
        print(tb)
        return None
    
def update_hotel_address_single(dict_hotel):
    try:
        conn = psycopg2.connect(**_conn_params)
        cursor = conn.cursor()
       
        hotel_id = dict_hotel['hotel_id']
        address = dict_hotel['address']
        cursor.execute(f"""
        UPDATE hotels 
        SET address = '{address}'
        WHERE id = '{hotel_id}'
    """)
        conn.commit()
        cursor.close()
        conn.close()
    except Exception as e:
        print('error in update_hotel_address')
        print(e)
        tb = traceback.format_exc()
        print(tb)
        return None

    

def main():
    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_argument("--headless")
    driver = webdriver.Chrome(options=chrome_options, service=ChromeService(ChromeDriverManager().install()))
    driver.maximize_window()
    try:
        stopwatch('on')
        list_dict_hotels = make_list_hotels()
        count_addresses = 0
        for dict_hotel in list_dict_hotels:
            hotel_link = dict_hotel['link']
            address = get_address(hotel_link, driver)
            dict_hotel['address'] = address
            dict_hotel=sql_quote_check(dict_hotel)
            count_addresses += 1
            update_hotel_address_single(dict_hotel)
        driver.close()
        print('count_addresses =', count_addresses)
        stopwatch('off')
    except Exception as e:
        print(e)
        tb = traceback.format_exc()
        print(tb)
        driver.close()
        return None
    
print('hotel_link_lookerNoUsa.py is imported')

if __name__ == '__main__':
    main()
